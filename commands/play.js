const { ApplicationCommandOptionType, EmbedBuilder } = require('discord.js');
const db = require("../mongoDB");
const errorNotifer = require("../functions");
module.exports = {
  name: "play",
  description: "Open music from other platforms.",
  permissions: "0x0000000000000800",
  options: [
    {
      name: "name",
      description: "Write your music name.",
      type: ApplicationCommandOptionType.String,
      required: true
    }
  ],
  voiceChannel: true,
  run: async (client, interaction) => {
    let lang = await db?.musicbot?.findOne({ guildID: interaction.guild.id })
    lang = lang?.language || client.language
    lang = require(`../languages/${lang}.js`);

    try {
      const name = interaction.options.getString('name')
      if (!name) return interaction.reply({ content: lang.msg59, ephemeral: true }).catch(e => { })

      await interaction.reply({ content: lang.msg61 }).catch(e => { })
      try {
        await client.player.play(interaction.member.voice.channel, name, {
          member: interaction.member,
          textChannel: interaction.channel,
          interaction
        })
      } catch (e) {
        await interaction.editReply({ content: lang.msg60, ephemeral: true }).catch(e => { })
      }
    } catch (e) {
      const errorNotifer = require("../functions.js")
      errorNotifer(client, interaction, e, lang)
    }
  },
};